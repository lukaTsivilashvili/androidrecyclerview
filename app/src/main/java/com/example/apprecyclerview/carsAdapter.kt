package com.example.apprecyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.apprecyclerview.databinding.RecyclerLayoutBinding


class carsAdapter(private var cars: List<carInfo>, private val context: Context) :
    RecyclerView.Adapter<carsAdapter.ItemViewholder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewholder {
        val itemView =
            RecyclerLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ItemViewholder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: ItemViewholder, position: Int) {

        holder.bind()

    }

    override fun getItemCount(): Int = cars.size

    inner class ItemViewholder(val binding: RecyclerLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: carInfo
        fun bind() {
            model = cars[adapterPosition]

            binding.makeTV.text = model.make
            binding.modelTV.text = model.model
            binding.engineTV.text = model.engine
            binding.yearTV.text = model.year.toString()
            binding.priceTV.text = model.power.toString()

            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(16))

            Glide.with(context)
                .load(model.url)
                .placeholder(R.drawable.ic_launcher_background)
                .apply(requestOptions)
                .into(binding.carImage)
        }
    }
}
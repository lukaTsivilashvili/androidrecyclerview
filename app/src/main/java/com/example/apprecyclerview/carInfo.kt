package com.example.apprecyclerview

data class carInfo(
    val make:String,
    val model:String,
    val engine:String,
    val year:Int,
    val power:Int,
    val url:String
)

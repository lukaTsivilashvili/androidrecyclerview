package com.example.apprecyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apprecyclerview.databinding.ActivityMainBinding
import com.example.apprecyclerview.databinding.RecyclerLayoutBinding

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: carsAdapter
    private lateinit var binding: ActivityMainBinding

    private val carsList = mutableListOf<carInfo>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        addInfo()
    }

    private fun init() {
        adapter = carsAdapter(carsList, this)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter

    }

    private fun addInfo() {

        carsList.add(
            carInfo(
                "Mercedes-Benz",
                "CLS 63 AMG",
                "4.4 L S63 twin-turbo V8",
                2017,
                518,
                "https://i.pinimg.com/originals/4a/8f/59/4a8f598bd392fe4a2c4c9ccce18eadb4.jpg"
            )
        )
        carsList.add(
            carInfo(
                "BMW",
                "M5 Competition",
                "M157 5.5L V8 BiTurbo",
                2018,
                617,
                "https://s35.wheelsage.org/format/picture/picture-preview-large/b/bmw/g-power/m5/g-power_bmw_m5_2_05e201c409010607.jpg"
            )
        )
        carsList.add(
            carInfo(
                "Audi",
                "R8",
                "5.2L V10",
                2020,
                562,
                "https://media.ed.edmunds-media.com/audi/r8/2020/oem/2020_audi_r8_coupe_v10-performance-quattro_fq_oem_6_1600.jpg"
            )
        )
        carsList.add(
            carInfo(
                "Lamborghini",
                "Huracan LP610-4",
                "5.2L V10",
                2020,
                610,
                "https://gtluxe.com/wp-content/uploads/2015/12/lamborghini-huracan-performante-spyder-gtluxe.jpg"
            )
        )

        carsList.add(
            carInfo(
                "Ferrari",
                "488 GTB",
                "3.9L V8",
                2017,
                661,
                "https://f7432d8eadcf865aa9d9-9c672a3a4ecaaacdf2fee3b3e6fd2716.ssl.cf3.rackcdn.com/C2299/U9430/IMG_57330-large.jpg"
            )
        )

        carsList.add(
            carInfo(
                "Toyota",
                "Supra MK-4 Black Widow",
                "2JZ GTE 3L",
                1996,
                610,
                "https://static.wixstatic.com/media/9be4e4_8647e1703a934fccab2e48b8dd486913~mv2.png/v1/fill/w_576,h_590,al_c/9be4e4_8647e1703a934fccab2e48b8dd486913~mv2.png"
            )
        )

        carsList.add(
            carInfo(
                "Dodge",
                "Charger",
                "7.2L V8",
                1996,
                376,
                "https://img.hmn.com/fit-in/450x253/filters:upscale()/stories/2018/08/342111.jpg"
            )
        )

        carsList.add(
            carInfo(
                "Rolls-Royce",
                "Cullinan",
                "6.8L V12",
                2020,
                563,
                "https://s3-prod.autonews.com/s3fs-public/CULLINANBLACKBADGE-MAIN_i.jpg"
            )
        )

        carsList.add(
            carInfo(
                "Aston Martin",
                "Valkyrie",
                "6.5L V12",
                2019,
                1000,
                "https://i.gaw.to/vehicles/photos/09/22/092245_2019_astonmartin_Valkyrie.jpg?1024x640"
            )
        )

        carsList.add(
            carInfo(
                "Mercedes-Benz",
                "SLS AMG",
                "6.2L V8",
                2015,
                583,
                "https://i.gaw.to/vehicles/photos/06/40/064057_2015_mercedesbenz_SLS_AMG.jpg"
            )
        )

        carsList.add(
            carInfo(
                "Toyota",
                "GT86",
                "2.4L Boxer",
                2021,
                260,
                "https://images.cdn.circlesix.co/image/1/1000/0/uploads/posts/2018/07/8c951b7788287cf87426a7c81487c233.jpg"
            )
        )

        carsList.add(
            carInfo(
                "Tesla",
                "Model S Performance",
                "Electric",
                2021,
                778,
                "https://i.gaw.to/vehicles/photos/40/25/402547-2021-tesla-model-s.jpg?1024x640"
            )
        )

        carsList.add(
            carInfo(
                "Mclaren",
                "720S",
                "4.0L V10",
                2021,
                710,
                "https://i.gaw.to/vehicles/photos/40/25/402518-2021-mclaren-720s.jpg?1024x640"
            )
        )

        carsList.add(
            carInfo(
                "Lamborghini",
                "Urus",
                "4.0L V8",
                2021,
                650,
                "https://i.gaw.to/vehicles/photos/40/24/402400-2021-lamborghini-urus.jpg?1024x640"
            )
        )

        carsList.add(
            carInfo(
                "Dodge",
                "Challenger SRT Hellcat Redeye",
                "6.2L V8",
                2021,
                797,
                "https://i.gaw.to/vehicles/photos/40/22/402276-2021-dodge-challenger.jpg?1024x640"
            )
        )

    }
}
